from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('', views.post_list, name = 'post_list'),
    path('post/<int:pk>/', views.post_detail, name = 'post_detail'),
    path('post_new',views.post_new, name = 'post_new'),
    path('add_like/<int:pk>/',views.add_like,name = 'add_like'),
    path('delete_post/<int:pk>/',views.delete_post,name = 'delete_post'),
    path('comment_new/<int:pk>/',views.comment_new,name = 'comment_new'),
]