from django.db import models
from django.utils import timezone


class Post(models.Model):
    title = models.CharField(max_length= 200)
    text = models.TextField()
    likes = models.IntegerField()

    created_date = models.DateTimeField(default = timezone.now)
    def __str__(self):
        return self.title
class Comment(models.Model):
    name = models.CharField(max_length=20)
    text = models.TextField()
    post = models.IntegerField()
    created_date = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.name




