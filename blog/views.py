from django.shortcuts import render,get_object_or_404,redirect
from blog.models import Post,Comment
from.forms import PostForm,CommentForm
# Create your views here.
def post_list(request):
    return render(request, 'blog/post_list.html', context={'posts': Post.objects.all()})
def post_detail(request, pk):
    post = get_object_or_404(Post, pk = pk)
    return render(request, 'blog/post_detail.html', {'post': post,'comments':Comment.objects.all(),'pk':pk})
def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.likes = 0
            post.save()

            return redirect('post_list')

    form = PostForm()
    return render(request,'blog/post_new.html', {'form': form})
def comment_new(request,pk):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = pk
            comment.save()
            return redirect('post_list')
    form = CommentForm()
    return render(request,'blog/comment_new.html',{'form':form})
def add_like(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.likes+=1
    post.save()
    return redirect('post_list')
def delete_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('post_list')



