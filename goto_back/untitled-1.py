from django.db import models
from django.utils import timezone
class Post(models.Model):
    title = models.CharField(max_length= 200)
    text = models.TextField()
    Likes = models.IntegerField()
    created_date = models.DateTimeField(default = timezone.now)
    